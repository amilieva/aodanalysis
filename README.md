AOD analysis and columnar processing

## How to run the code -- cmsRun only

cmsrel CMSSW_13_3_0

cd CMSSW_13_3_0/src/

cmsenv

mkdir AODAnalysis

cd AODAnalysis

git clone https://gitlab.cern.ch/amilieva/aodanalysis.git

scramv1 b

cd AODAnalysis/AODSkim/test

cmsRun RunGammaGammaLeptonLepton_cfg.py

## Useful CRAB commands

crab submit -c crab_cfg.py

crab status -d multicrab_TEST/crab_NanoAnalysis/

crab tasks