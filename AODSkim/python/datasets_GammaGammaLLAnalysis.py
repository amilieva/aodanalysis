import sys,os
JSONPATH = os.path.realpath(os.path.join(os.getcwd(),"../data"))

# The name of this file MUST contain the analysis name: datasets_<name>Analysis.py.
# E.g. NanoAOD_Hplus2taunuAnalysisSkim.py -> datasets_Hplus2taunuAnalysis.py

from Dataset import Dataset


datasets = {}

datasets["2023D"] = []
datasets["2023D"].append(Dataset('/Muon1/Run2023D-PromptReco-v1/AOD', dataVersion="data",lumiMask=os.path.join(JSONPATH,"PPS-July2023_track_only_AND_CMS_Golden.json")))


def getDatasets(dataVersion):
    if not dataVersion in datasets.keys():
        print("Unknown dataVersion",dataVersion,", dataVersions available",datasets.keys())
        sys.exit()
    return datasets[dataVersion]
