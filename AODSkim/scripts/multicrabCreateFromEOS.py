#!/usr/bin/env python

import os,sys
import datetime

from lumicalc import *

JSONPATH = "/eos/home-c/cmsdqm/www/CAF/certification/Collisions22/"

class Dataset:
    def __init__(self,eospaths,dataVersion,lumiMask="",name=""):
        self.eospaths = eospaths
        self.dataVersion = dataVersion
        self.lumiMask = lumiMask
        self.name = name

    def write(self,multicrabdir):
        os.mkdir(os.path.join(multicrabdir,self.name))
        os.mkdir(os.path.join(multicrabdir,self.name,'inputs'))
        os.mkdir(os.path.join(multicrabdir,self.name,'results'))
        if 'data' in self.dataVersion:
            if os.path.exists(self.lumiMask):
                cmd = "cp %s %s"%(self.lumiMask,os.path.join(multicrabdir,self.name,'inputs'))
                os.system(cmd)
            else:
                print("Lumimask",self.lumiMask,"not found, exiting..")
                sys.exit()

            lumidata = {}            
            inputFile = self.lumiMask
            logOUT = os.path.join(multicrabdir,self.name,"results","brilcalc.log")
            lumijson = os.path.join(multicrabdir,"lumi.json")
            if os.path.exists(lumijson):
                fIN = open(lumijson)
                lumidata = json.load(fIN)
                fIN.close()
            lumidata[self.name] = CallBrilcalc(inputFile,logOUT)
            f = open(lumijson, "wb")
            json.dump(lumidata, f, sort_keys=True, indent=2)
            f.close()

        fOUT = open(os.path.join(multicrabdir,'eosConfig_'+self.name+'.txt'),'w')
        cfg = {}
        cfg["name"] = self.name
        cfg["dataVersion"] = self.dataVersion
        cfg["eospath"] = self.eospaths
        json.dump(cfg, fOUT, indent=2)
        fOUT.close()

def main():
    multicrabName = "multicrab_JMENano_EOS_v2p1_Run2022CDEF"
    time = datetime.datetime.now().strftime("%Y%m%dT%H%M")
    multicrabName+= "_" + time

    datasets = []
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/DoubleMuon/JMENanoRun3_v2p0_Run2022C-PromptReco-v1/220905_183408/0000/"],
                            dataVersion="data",
                            lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraC_355862_357482_Golden.json"),
                            name="DoubleMuon_Run2022C_PromptReco_v1"))
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/Muon/JMENanoRun3_v2p0_Run2022C-PromptReco-v1/220905_183416/0000/",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/Muon/JMENanoRun3_v2p0_Run2022C-PromptReco-v1/220905_183416/0001/",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/Muon/JMENanoRun3_v2p0_Run2022C-PromptReco-v1/220905_183416/0002/"],
                            dataVersion="data",
                            lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraC_355862_357482_Golden.json"),
                            name="Muon_Run2022C_PromptReco_v1"))
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/Muon/JMENanoRun3_v2p0_Run2022D-PromptReco-v1/220905_183422/0000/"],
                            dataVersion="data",
                            lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraD_357538_357900_Golden.json"),
                            name="Muon_Run2022D_PromptReco_v1"))
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/Muon/JMENanoRun3_v2p0_Run2022D-PromptReco-v2/220905_183430/0000/"],              
                            dataVersion="data",
                            lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraD_357538_357900_Golden.json"),
                            name="Muon_Run2022D_PromptReco_v2"))
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/Muon/JMENanoRun3_v2p0_Run2022D-PromptReco-v3/220905_183436/0000/"],
                            dataVersion="data",
                            lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraD_357538_357900_Golden.json"),
                            name="Muon_Run2022D_PromptReco_v3"))
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022E-PromptReco-v1/221007_173948/0000",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022E-PromptReco-v1/221007_173948/0001",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022E-PromptReco-v1/221027_150102/0000",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022E-PromptReco-v1/221027_150102/0001"],
                            dataVersion="data",
                            lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraE_359022_360331_Golden.json"),
                            name="Muon_Run2022E_PromptReco_v1"))
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022F-PromptReco-v1/221027_143743/0000",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022F-PromptReco-v1/221027_143743/0001",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022F-PromptReco-v1/221111_165722/0000",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022F-PromptReco-v1/221111_165722/0001",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022F-PromptReco-v1/221111_165722/0002",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p1/Muon/JMENanoRun3_v2p1_Run2022F-PromptReco-v1/221111_165722/0003"],
                            dataVersion="data",
                            lumiMask=os.path.join(JSONPATH,"Cert_Collisions2022_eraF_360390_361580_Golden.json"),
                            name="Muon_Run2022F_PromptReco_v1"))

    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/JMENanoRun3_v2p0_MC22_122_ext1/220905_183646/0000/"],
                            dataVersion="mc",
                            name="DYJetsToLL_M_50_ext1"))
    datasets.append(Dataset(["/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/JMENanoRun3_v2p0_MC22_122_ext2/220905_183654/0000",
                             "/eos/cms/store/group/phys_jetmet/JMENanoRun3/v2p0/DYJetsToLL_M-50_TuneCP5_13p6TeV-madgraphMLM-pythia8/JMENanoRun3_v2p0_MC22_122_ext2/220905_183654/0001"],
                            dataVersion="mc",
                            name="DYJetsToLL_M_50_ext2"))

    if not os.path.exists(multicrabName):
        os.mkdir(multicrabName)

    for dset in datasets:
        dset.write(multicrabName)

    sys.stdout.write("Created %s\n"%multicrabName)

if __name__=="__main__":
    main()
