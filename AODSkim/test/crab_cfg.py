from WMCore.Configuration import Configuration
from CRABClient.UserUtilities import config
import os

config = Configuration()

config.section_("General")
config.General.requestName = 'AODAnalysis'
config.General.workArea = "multicrab_TEST"
config.General.transferOutputs = True
config.General.transferLogs=True

config.section_("JobType")
config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'RunGammaGammaLeptonLepton_cfg.py'
####config.JobType.scriptExe = 'NanoAOD_crab_script.sh'
config.JobType.inputFiles = ['RunGammaGammaLeptonLepton_cfg.py']
config.JobType.maxJobRuntimeMin = 5*1315
####config.JobType.sendPythonFolder  = True
config.JobType.outputFiles = ['output.root']

config.section_("Data")
config.Data.inputDataset = '/Muon1/Run2023D-PromptReco-v1/AOD'
config.Data.inputDBS = 'global'
config.Data.splitting = 'LumiBased'
config.Data.unitsPerJob = 10000
config.Data.outLFNDirBase = '/store/user/%s/TEST' % (os.environ.get('USER'))
#config.Data.outLFNDirBase = '/store/group/phys_jetmet/CRAB3_TransferData'
config.Data.publication = False

config.section_("Site")
config.Site.storageSite = "T2_FI_HIP"


